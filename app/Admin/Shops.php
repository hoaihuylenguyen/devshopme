<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Shops extends Model
{
    protected $primarykey = 'sh_id';

    public function Adress(){
        return $this->belongsTo(Adress::class,'sh_id','id_shop');
    }
}
