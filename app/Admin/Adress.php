<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Adress extends Model
{
    protected $primarykey = 'a_id';
    public function District(){
        return $this->belongsTo(District::class,'id_dis');
    }
    // public function Shops(){
    //     return $this->belongsTo(Shops::class,'sh_id');
    // }
}
