<?php

namespace App\Http\Controllers;

use App\Admin\Shop_pro;
use Illuminate\Http\Request;

class ShopProController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Shop_pro  $shop_pro
     * @return \Illuminate\Http\Response
     */
    public function show(Shop_pro $shop_pro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Shop_pro  $shop_pro
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop_pro $shop_pro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\Shop_pro  $shop_pro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop_pro $shop_pro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Shop_pro  $shop_pro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop_pro $shop_pro)
    {
        //
    }
}
