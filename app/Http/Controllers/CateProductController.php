<?php

namespace App\Http\Controllers;

use App\Admin\Cate_product;
use Illuminate\Http\Request;

class CateProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Cate_product  $cate_product
     * @return \Illuminate\Http\Response
     */
    public function show(Cate_product $cate_product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Cate_product  $cate_product
     * @return \Illuminate\Http\Response
     */
    public function edit(Cate_product $cate_product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\Cate_product  $cate_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cate_product $cate_product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Cate_product  $cate_product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cate_product $cate_product)
    {
        //
    }
}
