<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Setting;
use Faker\Generator as Faker;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        's_key' => $faker->title,
        's_value' => $faker->randomNumber(),
        's_display' => $faker->title,
        'del_flag' =>$faker->randomNumber(),
    ];
});
