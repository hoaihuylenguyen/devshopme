<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\District;
use Faker\Generator as Faker;

$factory->define(District::class, function (Faker $faker) {
    return [
        'd_name' => $faker->title,
        'd_code' => $faker->countryCode,
        'published' => $faker->boolean
    ];
});
