<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => 'shop@gmail.com',
        'email_verified_at' => now(),
        'password' => bcrypt('adminshop'), // password
        'remember_token' => Str::random(10),
        'u_picture' => $faker->imageUrl(),
        'birthday' => $faker->date('y-m-d'),
        'link' => $faker->imageUrl(),
        'phone' => $faker->phoneNumber,
        'active'=>1,
        'spam' => 1,
        'modifiled' => $faker->date(),
    ];
});
