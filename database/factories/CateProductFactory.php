<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Cate_product;
use Faker\Generator as Faker;

$factory->define(Cate_product::class, function (Faker $faker) {
    return [
        'cate_id' => rand(1,10),
        'id_prod' => rand(1,10)
    ];
});
