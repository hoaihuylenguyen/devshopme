<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->Increments('u_id');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('u_picture')->nullable();
            $table->date('birthday')->nullable();
            $table->string('link')->nullable();
            $table->string('phone')->nullable();
            $table->integer('active')->default(1);
            $table->integer('spam')->default(1);
            $table->dateTime('modifiled')->nullable();
            $table->string('u_address')->nullable();
            $table->integer('gender')->default(1);
            $table->integer('oauth_provider')->default(1);
            $table->integer('oauth_id')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
