<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\Admin\Adress::class,10)->create();
        factory(App\Admin\District::class,10)->create();
        factory(App\Admin\Setting::class,10)->create();
        factory(App\Admin\Shops::class,10)->create();
        factory(App\Admin\Cate_product::class,10)->create();
        factory(App\Admin\Category::class,10)->create();
        factory(App\Admin\Products::class,10)->create();
    }
}
