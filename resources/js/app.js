import {filter} from './filters';
require('./bootstrap');
window.Vue = require('vue');
// vue router
import route from './router';
//vue x
import vuex from 'vuex';
Vue.use(vuex);
import storeData from './store';
const store = new vuex.Store(storeData);

//v-form
import {Form, HasError, AlertError} from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

//sweet alert
import swal from 'sweetalert2'

window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', swal.stopTimer)
      toast.addEventListener('mouseleave', swal.resumeTimer)
    }
  })

window.toast = toast;

// Vue.component('pagi', require('./components/setting/Adress/list.vue').default);

// navigator
Vue.component('pagination', require('laravel-vue-pagination'));

//Editor
import 'v-markdown-editor/dist/index.css';
import Editor from 'v-markdown-editor'
Vue.use(Editor);

const app = new Vue({
    el: '#app',
    router: route,
    store,
    
});
