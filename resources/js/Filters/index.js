import Vue from 'vue';
//moment js
import moment from 'moment';
//vue filter cơ chế hoạt động như helper laravel
Vue.filter('timeformat', function(e){
  return moment().format('MMMM Do YYYY, h:mm:ss a');
})

Vue.filter('sortlength', function(text,length, subfix){
  return text.substring(0,length)+ subfix;
})